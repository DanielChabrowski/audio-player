# Foobar

[![pipeline status](https://gitlab.com/DanielChabrowski/audio-player/badges/master/pipeline.svg)](https://gitlab.com/DanielChabrowski/audio-player/commits/master)

### About
This project started as a quick check of how hard it would be to play music with QMediaPlayer. After forming a very simple GUI I had an idea to try and recreate the look of my favourite audio player [foobar2000](https://www.foobar2000.org/) which unfortunately is not available for linux that I am now using natively. As a result, there's a ton of features not implemented simply because I wasn't using them (like some menu items being empty) but it is enough for me to use it daily.

![foobar](https://i.imgur.com/nTYSdTz.png)

### Build from sources

#### Ubuntu
``
apt update && apt install git cmake ninja-build g++ python3 qt6-multimedia-dev qt6-base-dev libgl1-mesa-dev libtag1-dev
``
